# GitLab: Deploy to the Tactical Edge

In this training session, participants will learn how to:

- Bootstrap and install Flux CD in a Kubernetes cluster
- Connect the cluster to GitLab via the Agent for Kubernetes
- Deploy application manifests via GitOps

Please review the [issues](https://gitlab.com/zyates/rocky24/-/issues) for step-by-step instructions.